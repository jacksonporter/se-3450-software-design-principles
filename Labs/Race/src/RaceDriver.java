public class RaceDriver {
	public static void main(String[] args) {
		//Make a racer
		RaceParticipant racer = new RaceParticipant();
		
		//Make all the threads
		Thread tortoiseThread = new Thread(racer, "Tortoise");
		Thread hareThread = new Thread(racer, "Hare");
		Thread jaguarThread = new Thread(racer, "Jaguar");
		Thread robinThread = new Thread(racer, "Robin");
		
		try {
			tortoiseThread.start();
			Thread.sleep(500);
			
			hareThread.start();
			Thread.sleep(750);
			
			jaguarThread.start();
			Thread.sleep(250);
			
			robinThread.start();
		}catch(InterruptedException e)
		{
			System.out.println("Oh no! Interrupted Exception!!");
		}
	
		PlayByPlay play = PlayByPlay.getInstance();
		
		System.out.println(play);
		
	}
}