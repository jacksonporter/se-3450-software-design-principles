package Model;

import java.io.Serializable;

public abstract class BoothState implements Serializable {
	transient Booth booth;
	
	public BoothState(Booth booth) {
		this.booth = booth;
	}
	
	public abstract void wasClicked();
	
	public abstract void inCleaning();
	
	public abstract void wasCleaned();
	
	
	@Override
	public abstract String toString();
}
