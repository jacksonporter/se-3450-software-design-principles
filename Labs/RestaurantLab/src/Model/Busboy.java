package Model;

import java.util.ArrayList;

import View.ViewButton;

public class Busboy implements Runnable{
	ArrayList<Booth> booths;
	BusboyState offShiftState, backRoomState, cleaningState, currentState;
	int id;
	Restaurant restaurant;
	Booth booth;
	
	public Busboy(int id, Restaurant restaurant) {
		this.id = id;
		this.restaurant = restaurant;
		offShiftState = new OffShiftState(this);
		backRoomState = new BackRoomState(this);
		cleaningState = new CleaningState(this);
		currentState = offShiftState;
		
		this.booth = null;
	}
	
	public void setState(BusboyState state) {
		this.currentState = state;
		
		if(currentState.toString().equals("OffShiftState")) {
			restaurant.setBusBoyOffShift(this.id);
		}
		else if(currentState.toString().equals("BackRoomState")) {
			restaurant.setBusBoyBackRoom(this.id);
		}
		else if(currentState.toString().equals("CleaningState")) {
			restaurant.setBusBoyCleaning(this.id);
		}
	}
	
	public BusboyState getOffShiftState() {
		return this.offShiftState;
	}
	
	public BusboyState getBackRoomState() {
		return this.backRoomState;
	}
	
	public BusboyState getCleaningState() {
		return this.cleaningState;
	}
	
	public BusboyState getCurrentState() {
		return this.currentState;
	}
	
	public int getID() {
		return this.id;
	}
	
	public void offShift() {
		this.currentState.offShift();
	}
	
	public void backRoom() {
		this.currentState.backRoom();
	}
	
	public void cleaning() {
		this.currentState.cleaning();
	}
	
	public void setBooth(Booth booth) {
		this.booth = booth;
	}
	
	public void printToBusBoyButton(String message){
		restaurant.printToBusBoyButton(message);
	}
	

	@Override
	public void run() {
		System.out.println("Started busboy " + this.id + " thread.");
		
		while(true) {			
			if(this.booth != null) {
				System.out.println("busboy " + this.id + " has a booth to clean.");
				
				//booth.getCurrentState().inCleaning();
				System.out.println("MADE IT");
				
				try {
					this.currentState.cleaning();
					Thread.sleep(5000);
					
					booth.setState(booth.getReadyState());
					
					this.currentState.backRoom();
					this.setBooth(null);
					
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Crap. Busboy thread was interrupted.");
				}
			}
			else {
				//System.out.println("Sleeping for a sec.");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public Booth getBooth() {
		return this.booth;
	}
}
