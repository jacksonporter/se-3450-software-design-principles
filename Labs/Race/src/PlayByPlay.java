import java.util.ArrayList;

/*
 * This file contains a PlayByPlay object that is a singleton object, only one instance of this object can exist.
 */
public class PlayByPlay {
	private static PlayByPlay uniqueInstance = new PlayByPlay();
	private ArrayList<String> plays;
	private String winner;
	
	
	private PlayByPlay() 
	{
		winner = "***NO WINNER DEFINED***";
		plays = new ArrayList<String>();
	}
	
	public static PlayByPlay getInstance()
	{
		return uniqueInstance;
	}
	
	public void addPlay(String name, String location)
	{
		String toAdd = "RACING " + name + " is: " + location + " meters";
		
		plays.add(toAdd);
	}
	
	public void addWinner(String winner) 
	{
		this.winner = "The winner of the race was: " + winner + "!";		
	}
	
	@Override
	public String toString()
	{
		String toReturn = "";
		
		for(int i = 0; i < plays.size(); i++)
		{
			toReturn += plays.get(i) + "\n";
		}
		
		toReturn += winner + "\n";
		
		return toReturn;
	}
}
