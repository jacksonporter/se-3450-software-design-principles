import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class WashingMachineRemote extends UnicastRemoteObject implements WashingMachineRemoteInterface {
	WashingMachine machine;
	
	
	public WashingMachineRemote(WashingMachine machine) throws RemoteException {
		this.machine = machine;
	}


	@Override
	public String getStateString() throws RemoteException {
		return machine.getCurrentState().toString();
	}
	
}
