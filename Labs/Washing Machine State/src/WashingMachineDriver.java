import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import javax.swing.JOptionPane;

/**
 * This is the driver class to demo and show the Washing Machine State lab. 
 * @author jacksonporter
 *
 */
public class WashingMachineDriver {
	static MainWindow window;
	static WashingMachine washer;
	
	public static void main(String[] args) {
		System.out.println("Program running!");
		
		washer = new WashingMachine();
		
		System.out.println(washer);
		
		//Try the washer
		washer.insertQuarter();
		washer.insertQuarter();
		washer.insertQuarter();
		washer.insertQuarter();
		
		washer.startLoad();
		//washer.insertQuarter();
		//washer.cancelLoad();

		//window = new MainWindow(new ExitEvent(), new InsertQuarterEvent(), new EjectQuarterEvent(), new StartLoadEvent(), new CancelLoadEvent());
		
		try {
			WashingMachineRemoteInterface remote = new WashingMachineRemote(washer);
			
			//WashingMachineRemoteInterface stub = (WashingMachineRemoteInterface) UnicastRemoteObject.exportObject(remote, 0);
			
			WashingMachineRemoteInterface stub = (WashingMachineRemoteInterface) remote;
			
			Registry registry = LocateRegistry.getRegistry();
			
			registry.bind("remote1", stub);
			System.out.println("Server is ready.");
		} catch (RemoteException e) {
			System.out.println("RemoteException FOUND");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			System.out.println("AlreadyBoundException FOUND");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
		
	private static class ExitEvent implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("Closing program from exit menu item.");
			JOptionPane.showMessageDialog(window, "Closing Program.", "Exit Washing Machine", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
		
	}
	
	
	private static class InsertQuarterEvent implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			washer.insertQuarter();
			window.setState(washer.getCurrentState());
			window.setNumOfQuarters(washer.getNumQuarters());
		}
		
	}
	
	private static class EjectQuarterEvent implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			washer.ejectQuarter();
			window.setState(washer.getCurrentState());
			window.setNumOfQuarters(washer.getNumQuarters());
		}
		
	}
	
	private static class StartLoadEvent implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			washer.startLoad();
			window.setState(washer.getCurrentState());
			window.setNumOfQuarters(washer.getNumQuarters());
		}
		
	}
	
	private static class CancelLoadEvent implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			washer.cancelLoad();
			window.setState(washer.getCurrentState());
			window.setNumOfQuarters(washer.getNumQuarters());
		}
		
	}
}
