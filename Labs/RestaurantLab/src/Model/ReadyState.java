package Model;

public class ReadyState extends BoothState{

	public ReadyState(Booth booth) {
		super(booth);
	}
	
	@Override
	public void wasClicked() {
		booth.printToLog("Assigned Booth" + booth.getID() +" to customers.");
		booth.setState(booth.getInUseState());
	}

	@Override
	public void inCleaning() {
		booth.printToLog("Booth " + booth.getID() +": Cannot clean a ready table.");		
	}

	@Override
	public void wasCleaned() {
		booth.printToLog("Booth " + booth.getID() +": Table was already clean.");	
		
	}

	@Override
	public String toString() {
		return "ReadyState";
	}
}
