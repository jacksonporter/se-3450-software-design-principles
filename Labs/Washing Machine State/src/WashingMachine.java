import java.util.Timer;
import java.util.TimerTask;

/**
 * This is the washing machine class that is a washing machine that contains states of the washing machine and does operations. 
 * @author jacksonporter
 *
 */
public class WashingMachine {
	State notFullyPaid, hasPaid, inUse, rinse, wash, spin_down, broken_down, current;
	int numOfQuarters;
	Timer timer; 

	public WashingMachine() {
		//Instantiate global variables 
		notFullyPaid = new NotFullyPaidState(this); //not fully paid state
		hasPaid = new HasPaidState(this); //has paid state
		inUse = new InUseState(this); //in use state
		rinse = new RinseState(this); //rinse state
		wash = new WashState(this); //wash state
		spin_down = new Spin_DownState(this); //spin down state
		
		current = notFullyPaid; //Set the initial state to not fully paid
		
		numOfQuarters = 0; //set the numbers of quarters to zero
		
		timer = new Timer(); //create a timer object to be used when washing
	}
	
	/*
	 * Methods to control washing machine.
	 */

	public void insertQuarter() {
		current.insertQuarter();
	}

	
	public void ejectQuarter() {
		current.ejectQuarter();
	}

	
	public void startLoad() {
		current.startLoad();
	}

	
	public void cancelLoad() {
		current.cancelLoad();
	}
	
	
	
	/*
	 * Create getters for all of the states:
	 */
	
	public State getNotFullyPaidState() {
		return notFullyPaid;
	}
	
	public State getHasPaidState() {
		return hasPaid;
	}
	
	public State getInUseState() {
		return inUse;
	}
	
	public State getRinseState() {
		return rinse;
	}
	
	public State getWashState() {
		return wash;
	}
	
	public State getSpin_DownState() {
		return spin_down;
	}
	
	/*
	 * Eventually add a broken down state
	 */
	
	
	
	public int insertQuarterFromState() {
		numOfQuarters += 1;	
		return numOfQuarters;
	}
	
	public int ejectQuarterFromState() {
		numOfQuarters -= 1;
		return numOfQuarters;
	}
	
	public void setState(State state) {
		this.current = state;
		this.current.doOperation(); //do internal operations on state change if needed.
	}
	
	public State getCurrentState()
	{
		return current;
	}
	
	public int getNumQuarters() {
		return numOfQuarters;
	}
	
	public void addTaskToTimer(TimerTask task, int milliseconds) {
		timer.schedule(task, milliseconds);
	}
	
	public void timerCancel() {
		timer.cancel();
	}
	
	@Override
	public String toString() {
		return "I am a washer.";
	}

	public void interuptAllTasks(int seconds) {
		try {
			timer.schedule(new KillTimer(), seconds * 1000);
		}catch(java.lang.IllegalStateException e) {
			//woops! Timer has already been cancelled.
		}
	}
	
	
	private class KillTimer extends TimerTask{

		@Override
		public void run() {
			timer.cancel();
			timer = new Timer();
		}
		
	}
	
}
