package View;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import Controller.Controller;

/**
 * Main view to display entire restaurant in one view. 
 * @author jackson.porter
 *
 */
public class FullRestaurantView {
	private static final String JTextArea = null;
	private JFrame frame;
	private Controller controller;
	private ArrayList<JButton> booths;
	private ArrayList<JButton> banquets;
	private ArrayList<JButton> busboys;
	private JTextArea logArea;
	JScrollPane scrollPane;
	
	/**
	 * Creates a basic fullrestaurantview.
	 * @param consoleLog 
	 */
	public FullRestaurantView(Controller controller) {
		this.controller = controller;
		
		frame = new JFrame("Restaurant Lab");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImageIcon icon = new ImageIcon("src/logo.png");
		frame.setIconImage(icon.getImage());
		
		booths = new ArrayList<JButton>();
		banquets = new ArrayList<JButton>();
		busboys = new ArrayList<JButton>();
		
		logArea = new JTextArea("test test");
		
		//this.addComponentsToPane(frame.getContentPane());
		this.addComponentsToPaneGridLayout(frame.getContentPane());
		
		frame.pack();
		frame.setSize(frame.getPreferredSize());
	}
	
	private Controller getController()
	{
		return controller;
	}
	
	private void addComponentsToPaneGridLayout(Container contentPane) {
		contentPane.setLayout(new GridLayout(0, 2));
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new GridLayout(4, 0));
		
		/** 
		 * Create a menu bar, add basic functionality to the menu.
		 */
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ExitEvent(this));
		JMenuItem setOffShift = new JMenuItem("Busboy: Set Off Shift");
		setOffShift.addActionListener(new BusBoyEvent(this.controller));
		JMenuItem setOnShift = new JMenuItem("Busboy: Set On Shift");
		setOnShift.addActionListener(new BusBoyEvent(this.controller));
		fileMenu.add(exit);
		fileMenu.add(setOffShift);
		fileMenu.add(setOnShift);
		menuBar.add(fileMenu);
		this.frame.setJMenuBar(menuBar);
		
		
		JPanel panel1 = new JPanel();		
		ViewButton booth1 = new ViewButton("Booth 1", 1);
		booth1.setBackground(Color.cyan);
		booth1.setFont(new Font("Impact", Font.BOLD, 32));
		booth1.setHorizontalAlignment(SwingConstants.CENTER);
		booths.add(booth1);
		JPanel tempPanel1 = new JPanel();
		JPanel tempPanel2 = new JPanel();
		ViewButton booth3 = new ViewButton("Booth 3", 3);
		booth3.setBackground(Color.cyan);
		booth3.setFont(new Font("Impact", Font.BOLD, 32));
		booth3.setHorizontalAlignment(SwingConstants.CENTER);
		booths.add(booth3);
		JPanel tempPanel3 = new JPanel();
		JPanel tempPanel4 = new JPanel();
		panel1.setLayout(new GridLayout(1, 6));
		panel1.add(booth1);
		panel1.add(tempPanel1);
		panel1.add(tempPanel2);
		panel1.add(booth3);
		panel1.add(tempPanel3);
		panel1.add(tempPanel4);
		leftPanel.add(panel1);
		
		JPanel panel2 = new JPanel();		
		ViewButton booth2 = new ViewButton("Booth 2", 2);
		booth2.setBackground(Color.cyan);
		booth2.setFont(new Font("Impact", Font.BOLD, 32));
		booth2.setHorizontalAlignment(SwingConstants.CENTER);
		ViewButton booth4 = new ViewButton("Booth 4", 4);
		booth4.setBackground(Color.cyan);
		booth4.setFont(new Font("Impact", Font.BOLD, 32));
		booth4.setHorizontalAlignment(SwingConstants.CENTER);
		booths.add(booth2);
		booths.add(booth4);
		JPanel tempPanel5 = new JPanel();
		JPanel tempPanel6 = new JPanel();
		JPanel tempPanel7 = new JPanel();
		JPanel tempPanel8 = new JPanel();
		panel2.setLayout(new GridLayout(1, 6));
		panel2.add(booth2);
		panel2.add(tempPanel5);
		panel2.add(tempPanel6);
		panel2.add(booth4);
		panel2.add(tempPanel7);
		panel2.add(tempPanel8);
		leftPanel.add(panel2);
		
		
		JPanel panel3 = new JPanel();	
		ViewButton banquet1 = new ViewButton("Banquet 1", 5);
		banquet1.setBackground(Color.cyan);
		banquet1.setFont(new Font("Impact", Font.BOLD, 32));
		banquet1.setHorizontalAlignment(SwingConstants.CENTER);
		ViewButton banquet2 = new ViewButton("Banquet 2", 6);
		banquet2.setBackground(Color.cyan);
		banquet2.setFont(new Font("Impact", Font.BOLD, 32));
		banquet2.setHorizontalAlignment(SwingConstants.CENTER);
		booths.add(banquet1);
		booths.add(banquet2);
		JPanel tempPanel9 = new JPanel();
		JPanel tempPanel10 = new JPanel();
		panel3.setLayout(new GridLayout(1, 4));
		panel3.add(banquet1);
		panel3.add(tempPanel9);
		panel3.add(banquet2);
		panel3.add(tempPanel10);
		leftPanel.add(panel3);
		
		
		
		//Busboy				
		ViewButton busboy1 = new ViewButton("Bus Boy 1: ", 1);
		busboy1.setBackground(Color.gray);
		busboy1.setFont(new Font("Impact", Font.BOLD, 28));
		busboy1.setHorizontalAlignment(SwingConstants.LEFT);
		busboys.add(busboy1);
		leftPanel.add(busboy1);
		
		//Add left panel
		contentPane.add(leftPanel);
		
		logArea.setText("");
		scrollPane = new JScrollPane(logArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		//scrollPane.setSize(new Dimension(500, 300));
		contentPane.add(scrollPane);
		
		
						
		
		//Add click ability to all booths and banquets 
		for(JButton button: booths) {
			button.addActionListener(new BoothEvent(controller));
		}
	}
		
	public void addTextToLog(String text) {		
		String currentText = logArea.getText();
		logArea.append(text + "\n");
	}
	
	public void setVisible(boolean b) {
		frame.setVisible(b);
	}
	
	public void setBoothReady(int id) {
		for(int i = 0; i < booths.size(); i++) {
			ViewButton vb = (ViewButton) booths.get(i);
			
			if(vb.getID() == id) {
				vb.setBackground(Color.decode("#00ff40"));
				break;
			}
		}
	}
	
	public void setBoothInUse(int id) {		
		for(int i = 0; i < booths.size(); i++) {
			ViewButton vb = (ViewButton) booths.get(i);
			
			if(vb.getID() == id) {
				vb.setBackground(Color.decode("#ffff00"));
				break;
			}
		}
	}
	
	public void setBoothNeedsCleaning(int id) {
		for(int i = 0; i < booths.size(); i++) {
			ViewButton vb = (ViewButton) booths.get(i);
			
			if(vb.getID() == id) {
				vb.setBackground(Color.decode("#ff4000"));
				break;
			}
		}
	}
	
	public void setBoothIsBeingCleaned(int id) {
		for(int i = 0; i < booths.size(); i++) {
			ViewButton vb = (ViewButton) booths.get(i);
			
			if(vb.getID() == id) {
				vb.setBackground(Color.decode("#bfff00"));
				break;
			}
		}
	}
	
	public void setBusBoyOffShift(int id) {
		for(int i = 0; i < busboys.size(); i++) {
			ViewButton vb = (ViewButton) busboys.get(i);
			
			if(vb.getID() == id) {
				vb.setText("Busboy " + id + " : " + "Off Shift");
				break;
			}
		}
	}
	
	public void setBusBoyBackRoom(int id) {
		
		for(int i = 0; i < busboys.size(); i++) {
			ViewButton vb = (ViewButton) busboys.get(i);
			
			if(vb.getID() == id) {
				vb.setText("Busboy " + id + " : " + "Back Room");
				break;
			}
		}
	}
	
	public void setBusBoyCleaning(int id) {
		for(int i = 0; i < busboys.size(); i++) {
			ViewButton vb = (ViewButton) busboys.get(i);
			
			if(vb.getID() == id) {
				vb.setText("Busboy " + id + " : " + "Cleaning");
				break;
			}
		}
	}
	
	
	private static class ExitEvent implements ActionListener{
		FullRestaurantView view;
		
		public ExitEvent(FullRestaurantView view) {
			super();
			this.view = view;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JOptionPane.showMessageDialog(null, "Closing Program", "Exit Restaurant", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
		
	}
	
	private static class BusBoyEvent implements ActionListener{
		Controller controller;
		
		public BusBoyEvent(Controller controller) {
			super();
			this.controller = controller;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JMenuItem menuItem = (JMenuItem) e.getSource();
			
			if(menuItem.getText().equals("Busboy: Set Off Shift")) {
				controller.setAllOffShift();
			}
			else if(menuItem.getText().equals("Busboy: Set On Shift")){
				controller.setAllOnShift();
			}
		}
	}
	
	private static class BoothEvent implements ActionListener{
		Controller controller;
		
		public BoothEvent(Controller controller) {
			super();
			this.controller = controller;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ViewButton button = (ViewButton) e.getSource();
			controller.boothWasClicked(button.getID());
		}
	}

	public void printToBusBoyButton(String message) {
		System.out.println("BUSBOY message: " + message);
		
	}	
}
