package Model;

public class BeingCleanedState extends BoothState{

	public BeingCleanedState(Booth booth) {
		super(booth);
	}
	
	@Override
	public void wasClicked() {
		booth.printToLog("Booth " + booth.getID() +": Table needs to be cleaned before doing anything else!");	
	}

	@Override
	public void inCleaning() {
		booth.printToLog("Booth " + booth.getID() +": Table is already being cleaned!");		
	}

	@Override
	public void wasCleaned() {
		booth.printToLog("Booth " + booth.getID() +": Table is now ready for use.");
		booth.setState(booth.getInUseState());
	}

	@Override
	public String toString() {
		return "BeingCleanedState";
	}
}


