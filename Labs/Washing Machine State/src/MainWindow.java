import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;

public class MainWindow extends JFrame {
	ActionListener exitEvent, insertQuarterEvent, ejectQuarterEvent, startLoadEvent, canceLoadEvent;
	JLabel numOfQuarters, currentState;
	
	public MainWindow(ActionListener exitEvent, ActionListener insertQuarterEvent, ActionListener ejectQuarterEvent, ActionListener startLoadEvent, ActionListener canceLoadEvent) {
		super("Washing Machine");
		this.exitEvent = exitEvent;
		this.insertQuarterEvent = insertQuarterEvent;
		this.ejectQuarterEvent = ejectQuarterEvent;
		this.startLoadEvent = startLoadEvent;
		this.canceLoadEvent = canceLoadEvent;
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		addComponentsToPane(this.getContentPane());
		
		//Set icon for the frame
		ImageIcon icon = new ImageIcon("smallicon.png");
		this.setIconImage(icon.getImage());
		
		this.pack();
		this.setVisible(true);
	}
	
	public void setState(State state) {
		currentState.setText(state.toString());
	}
	
	public void setNumOfQuarters(int numOfQuarters) {
		this.numOfQuarters.setText("" + numOfQuarters);
	}

	private void addComponentsToPane(Container contentPane) {
		this.setLayout(new GridBagLayout()); //Set frame layout
				
		//Create a menu bar
		JMenuBar menuBar = new JMenuBar();
		
		//Create a menu that will go onto the menu bar
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
			//Create a menu item for the file menu
			JMenuItem exit = new JMenuItem("Exit");
			exit.addActionListener(exitEvent);
			fileMenu.add(exit);
			
		//Add the menu to the frame. 
		this.setJMenuBar(menuBar);
		
		
		
		
		gbc.insets = new Insets(top, left, bottom, right);
		
		JLabel titletop = new JLabel("Washing Machine");
		titletop.setForeground(Color.darkGray);
		titletop.setFont(new Font("Impact", Font.BOLD, 32));
		titletop.setHorizontalAlignment(SwingConstants.CENTER);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(titletop, gbc);
		
		JButton quarterInsertButton = new JButton("Insert 1 Quarter");
		quarterInsertButton.addActionListener(this.insertQuarterEvent);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(quarterInsertButton, gbc);
		
		JButton quarterEjectButton = new JButton("Eject 1 Quarter");
		quarterEjectButton.addActionListener(this.ejectQuarterEvent);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(quarterEjectButton, gbc);
		
		JButton startLoadButton = new JButton("Start Load");
		startLoadButton.addActionListener(this.startLoadEvent);
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(startLoadButton, gbc);
		
		JButton cancelWashButton = new JButton("Cancel Wash");
		cancelWashButton.addActionListener(this.canceLoadEvent);
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(cancelWashButton, gbc);
		
	
		JLabel currentStateLabel = new JLabel("Current State: ");
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(currentStateLabel, gbc);
		
		currentState = new JLabel("N/A");
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(currentState, gbc);
		
		JLabel numOfQuartersLabel = new JLabel("Number of Quarters: ");
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(numOfQuartersLabel, gbc);
		
		numOfQuarters = new JLabel("N/A");
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(numOfQuarters, gbc);
		
		JLabel jlabeltext = new JLabel();
		ImageIcon icon = new ImageIcon("smallicon.png");
		jlabeltext.setIcon(icon);
		
		//gBC.weightx = 3;
		gbc.gridx = 2;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridheight = 7;
		contentPane.add(jlabeltext, gbc);
	}

}
