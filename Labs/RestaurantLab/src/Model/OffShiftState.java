package Model;

public class OffShiftState extends BusboyState {
		
	public OffShiftState(Busboy busboy) {
		super(busboy);
	}

	@Override
	public void offShift() {
		System.out.println("Cannot set busboy " + this.busboy.getID() + " to offShift, he is already off shift!");
	}

	@Override
	public void backRoom() {
		busboy.setState(busboy.getBackRoomState());
	}

	@Override
	public void cleaning() {
		System.out.println("Cannot set busboy " + this.busboy.getID() + " to offShift, he is must go to the backroom first!");
	}
	
	@Override
	public String toString() {
		return "OffShiftState";
	}

}
