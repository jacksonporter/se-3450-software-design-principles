import java.text.DecimalFormat;

public class RaceParticipant implements Runnable {
	DecimalFormat df = new DecimalFormat("0.00");
	public static String daWinner;
	public PlayByPlay play = PlayByPlay.getInstance();
	
	public void race() {
			String threadName = Thread.currentThread().getName();
			double stepSize = 0.00;
			
			if(threadName.equals("Tortoise")) 
			{
				stepSize = 0.4;
			}
			else if(threadName.equals("Hare"))
			{
				stepSize = 0.5;
			}
			else if(threadName.equals("Jaguar"))
			{
				stepSize = 1.5;
			}
			else if(threadName.equals("Robin"))
			{
				stepSize = 2.0;
			}
			else
			{
				System.out.println("System we have a problem.");
				System.exit(1);
			}
		
			for(double distance=0;distance<=50.0;distance=Double.parseDouble(df.format(distance+stepSize))){
			
			try {
				Thread.sleep(10);
			}catch(InterruptedException e)
			{
				System.out.println("Oh no! Interrupted Exception!!");
			}

		
			if(this.daWinner!=null) {
				break;
				//System.out.println("I've LOST: "+Thread.currentThread().getName()+ " is: "+distance +" meters");
				
			}else {
				//System.out.println("RACING "+Thread.currentThread().getName()+ " is: "+distance +" meters");
				play.addPlay(Thread.currentThread().getName(), "" + distance);
					
			}
			boolean isRaceWon = this.isRaceWon(distance);
			if(isRaceWon){
				break;
			}
		}
	}
 
	private boolean isRaceWon(double totalDistanceCovered){
		boolean isRaceWon =  false;
		if((RaceParticipant.daWinner==null )&&(totalDistanceCovered>=50.0)){
				RaceParticipant.daWinner = Thread.currentThread().getName();
				//System.out.println("Winner is :"+RaceParticipant.daWinner);
				play.addWinner(RaceParticipant.daWinner);
				
				isRaceWon = true;
			}
			/*need to stop loop if other party has won*/
		    if (totalDistanceCovered>=50){
				isRaceWon = true;
			}
		return isRaceWon;
		}
 
	@Override
	public void run() {
		this.race();
 
	}
 
}
 
