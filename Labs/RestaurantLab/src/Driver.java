
import Controller.Controller;
import View.FullRestaurantView;

/**
 * This driver class contains the driver to run the main Restaurant Lab program. 
 * @author jackson.porter
 *
 */
public class Driver {

	public static void main(String[] args) {		
		Controller controller = new Controller();		
	}
}
