# SE 3450 - Software Design Principles

![](https://www.snow.edu/pr/brand/images/signature.jpg)

##Software Design Principles
#####The HeadFirst and Demos folder contains code from the repository: https://github.com/bethrobson/Head-First-Design-Patterns

######Content from the course, including course work and other misc. work. Work in this repository is in the Java programming language.

This class taught how to use various software patterns in order to create more effective and change-simpathetic code in the future.`

Break down of applications/projects in the **lab** folder:

- Race: The tortoise and the hare ... and the jaguar and robin. This program practices multithreading and sharing information between threads, but keeping some information private.

- RestaurantLab: Practice of the MVC pattern via the idea of a restaurant. 

- Washing Machine State: Practice of the State Pattern via the idea of a Washing Machine.
