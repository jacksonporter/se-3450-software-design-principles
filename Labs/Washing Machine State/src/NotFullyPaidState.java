public class NotFullyPaidState extends State {

	public NotFullyPaidState(WashingMachine washingMachine) {
		super(washingMachine);
	}

	@Override
	public void insertQuarter() {
		int numOfQuarters = washingMachine.insertQuarterFromState();
		
		if(numOfQuarters < 4) {
			System.out.println("You inserted a quarter! There are now a total of " + numOfQuarters + " quarters in the machine.");
			//Don't change state as we are not fully paid.
		}
		else {
			System.out.println("You inserted a quarter! There are now a total of " + numOfQuarters + " quarters in the machine and is ready to start.");
			washingMachine.setState(washingMachine.getHasPaidState());
		}
	}

	@Override
	public void ejectQuarter() {
		int numOfQuarters = washingMachine.getNumQuarters();
		
		if(numOfQuarters < 1) {
			System.out.println("You can't eject a quarter! There are no quarters in the machine.");
			//Don't change state as we are not fully paid.
		}
		else {
			numOfQuarters = washingMachine.ejectQuarterFromState();
			System.out.println("You ejected a quarter! There are now a total of " + numOfQuarters + " quarters in the machine.");
			//Don't change state as we won't be fully paid.
		}
	}

	@Override
	public void startLoad() {
		System.out.println("Sorry, you can't start a load. You haven't paid the full amount. Please insert " + (4 - washingMachine.getNumQuarters()) + " more quarters.");
		//Do change state as we are not fully paid still.
	}

	@Override
	public void cancelLoad() {
		System.out.println("Sorry, you can't cancel a load when you haven't started one!. Please insert " + (4 - washingMachine.getNumQuarters()) + " more quarters and start your load.");
		//Do change state as we are not fully paid still.		
	}

	@Override
	public void doOperation() {
		//nothing to do!	
		//System.out.println("NOT FULLY PAID STATE WAS SET");
		
		washingMachine.interuptAllTasks(6000);
	}
	
	@Override
	public String toString() {
		return "Not Fully Paid";
	}

}
