package View;

import javax.swing.JButton;

public class ViewButton extends JButton {
	private int id;
	
	public ViewButton(String m, int id) {
		super(m);
		this.id = id;
	}
	
	public int getID() {
		return this.id;
	}
	
	@Override
	public String toString() {
		return "" + id;
	}
}
