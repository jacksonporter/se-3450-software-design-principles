package Model;

public class CleaningState extends BusboyState {
		
	public CleaningState(Busboy busboy) {
		super(busboy);
	}

	@Override
	public void offShift() {
		System.out.println("Cannot set busboy " + this.busboy.getID() + " to offshift, he is cleaning!");
	}

	@Override
	public void backRoom() {
		busboy.setState(busboy.getBackRoomState());
	}

	@Override
	public void cleaning() {
		System.out.println("Cannot set busboy " + this.busboy.getID() + " to cleaning, he is already cleaning!");
	}

	@Override
	public String toString() {
		return "CleaningState";
	}
}
