package Model;

public class NeedsCleaningState extends BoothState{

	public NeedsCleaningState(Booth booth) {
		super(booth);
	}
	
	@Override
	public void wasClicked() {
		booth.printToLog("Booth " + booth.getID() +": Table needs to be cleaned before doing anything else!");		
	}

	@Override
	public void inCleaning() {
		booth.printToLog("Booth" + booth.getID() +" is now being cleaned.");
		booth.setState(booth.getBeingCleanedState());
	}

	@Override
	public void wasCleaned() {
		booth.printToLog("Booth " + booth.getID() +": Must clean before re-use.");	
		
	}

	@Override
	public String toString() {
		return "NeedsCleaningState";
	}
}
