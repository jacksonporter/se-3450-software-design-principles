package Model;

public class BackRoomState extends BusboyState {
		
	public BackRoomState(Busboy busboy) {
		super(busboy);
	}

	@Override
	public void offShift() {
		busboy.setState(busboy.getOffShiftState());
	}

	@Override
	public void backRoom() {
		System.out.println("Cannot set busboy " + this.busboy.getID() + " to backroom, he is already in the backroom!");
	}

	@Override
	public void cleaning() {
		busboy.setState(busboy.getCleaningState());
	}

	@Override
	public String toString() {
		return "BackRoomState";
	}
}
