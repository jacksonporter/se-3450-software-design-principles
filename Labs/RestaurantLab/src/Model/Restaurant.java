package Model;

import java.awt.Color;
import java.util.ArrayList;

import View.FullRestaurantView;
import View.ViewButton;

public class Restaurant {
	FullRestaurantView view;
	ArrayList<Booth> booths;
	ArrayList<Busboy> busboys;

	public Restaurant(FullRestaurantView view) {
		this.view = view;
		
		booths = new ArrayList<Booth>();
		busboys = new ArrayList<Busboy>();
		
		//Manual restaurant creation
		
		booths.add(new Booth(1, this));
		booths.add(new Booth(2, this));
		booths.add(new Booth(3, this));
		booths.add(new Booth(4, this));
	    booths.add(new Booth(5, this));
		booths.add(new Booth(6, this));
		
		busboys.add(new Busboy(1, this));
		
		//Start busboys threads
		Thread bb1 = new Thread(busboys.get(0), "BusBoy1");
		bb1.start();
	}

	public void boothWasClicked(int id) {		
		for(int i = 0; i < booths.size(); i++) {
			Booth booth = booths.get(i);
			
			if(booth.getID() == id) {
				booth.wasClicked();
			}
		}
	}
	
	public void setBoothReady(int id) {
		view.setBoothReady(id);
	}
	
	public void setBoothInUse(int id) {
		view.setBoothInUse(id);
	}
	
	public void setBoothNeedsCleaning(int id) {
		view.setBoothNeedsCleaning(id);

		//Tell busboy to do some work if available
		for(int i = 0; i < booths.size(); i++) {
			if(booths.get(i).getCurrentState().toString().equals("NeedsCleaningState")) {
				System.out.println("In condition");
				for(int k = 0; k < busboys.size(); k++) {
					System.out.println("In bus boys, K: " + k);
					if(busboys.get(k).getCurrentState().toString().equals("BackRoomState")) {
						System.out.println("Found a bus boy!");
						busboys.get(k).setBooth(booths.get(i));
						break;
					}
					if(k == (busboys.size() - 1)) {
						System.out.println("I'll wait a sec.");
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						k = -1;
					}
				}
			}
		}
	}
	
	public void setBoothIsBeingCleaned(int id) {
		view.setBoothIsBeingCleaned(id);
	}
	
	public void setBusBoyOffShift(int id) {
		view.setBusBoyOffShift(id);
	}
	
	public void setBusBoyBackRoom(int id) {
		view.setBusBoyBackRoom(id);
	}
	
	public void setBusBoyCleaning(int id) {
		view.setBusBoyCleaning(id);
	} 
	
	public void printToLog(String message) {
		view.addTextToLog(message);
	}

	public void printToBusBoyButton(String message) {
		view.printToBusBoyButton(message);
		
	}

	public void setAllOffShift() {
		for(int i = 0; i < busboys.size(); i++) { 
			busboys.get(i).getCurrentState().offShift();
		}
	}

	public void setAllOnShift() {
		for(int i = 0; i < busboys.size(); i++) {
			busboys.get(i).getCurrentState().backRoom();
		}
	}

}
