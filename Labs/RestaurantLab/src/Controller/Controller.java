package Controller;

import Model.Restaurant;
import View.FullRestaurantView;

public class Controller {
	Restaurant model;
	FullRestaurantView view;
	
	public Controller() {
		view = new FullRestaurantView(this);
		view.setVisible(true);
		
		model = new Restaurant(view);	
	}

	public void boothWasClicked(int id) {
		model.boothWasClicked(id);
	}

	public void setAllOffShift() {
		model.setAllOffShift();
	}

	public void setAllOnShift() {
		model.setAllOnShift();
	}
}
