package Model;

public class InUseState extends BoothState{

	public InUseState(Booth booth) {
		super(booth);
	}
	
	@Override
	public void wasClicked() {
		booth.printToLog("Booth" + booth.getID() +" customers left");
		booth.setState(booth.getNeedsCleaningState());
	}

	@Override
	public void inCleaning() {
		booth.printToLog("Booth " + booth.getID() +": Cannot clean a table with customers.");		
	}

	@Override
	public void wasCleaned() {
		booth.printToLog("Booth " + booth.getID() +": Table is in use, cannot be cleaned.");	
	}

	@Override
	public String toString() {
		return "InUseState";
	}
}
