
package Model;

import javax.swing.JOptionPane;

public class Booth {
	Restaurant restaurant;
	BoothState readyState, inUseState, needsCleaningState, beingCleanedState;
	BoothState currentState;
	int id;
	
	public Booth(int id, Restaurant restaurant) {
		this.restaurant = restaurant;
		
		readyState = new ReadyState(this);
		inUseState = new InUseState(this);
		needsCleaningState = new NeedsCleaningState(this);
		beingCleanedState = new BeingCleanedState(this);
		
		this.id = id;
		
		setState(readyState);
	}
	
	public BoothState getCurrentState() {
		return currentState;
	}
	
	public void setState(BoothState state) {
		this.currentState = state;
				
		if(currentState.toString().equals("ReadyState")) {
			restaurant.setBoothReady(this.id);
		}
		else if(currentState.toString().equals("InUseState")) {
			restaurant.setBoothInUse(this.id);
		}else if(currentState.toString().equals("NeedsCleaningState")) {
			restaurant.setBoothNeedsCleaning(this.id);
		}else if(currentState.toString().equals("BeingCleanedState")) {
			restaurant.setBoothIsBeingCleaned(this.id);
		}
		else {
			JOptionPane.showMessageDialog(null, "ERROR IN STATE. CLOSING", "ERROR", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}
	
	public BoothState getReadyState() {
		return this.readyState;
	}
	
	public BoothState getInUseState() {
		return this.inUseState;
	}
	
	public BoothState getNeedsCleaningState() {
		return this.needsCleaningState;
	}
	
	public BoothState getBeingCleanedState() {
		return this.beingCleanedState;
	}
	
	public int getID() {
		return this.id;
	}
	
	
	public void wasClicked(){
		currentState.wasClicked();
	}
	
	public void inCleaning() {
		currentState.inCleaning();
	}
	
	public void wasCleaned() {
		currentState.wasCleaned();
	}
	
	public void printToLog(String message){
		restaurant.printToLog(message);
	}
}
