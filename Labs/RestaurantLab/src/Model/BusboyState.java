package Model;

public abstract class BusboyState {
	transient Busboy busboy;
	
	public BusboyState(Busboy busboy) {
		this.busboy = busboy;
	}
	
	public abstract void offShift();
	
	public abstract void backRoom();
	
	public abstract void cleaning();
}
