import java.rmi.Remote;
import java.rmi.RemoteException;

public interface WashingMachineRemoteInterface extends Remote {
	String getStateString() throws RemoteException;
}
