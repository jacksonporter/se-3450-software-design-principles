import java.util.Timer;
import java.util.TimerTask;

public class WashState extends State{
	
	public WashState(WashingMachine washingMachine) {
		super(washingMachine);
		
	}
	
	@Override
	public void insertQuarter() {
		System.out.println("Cannot insert quarter when washer is in use. Please wait or cancel the load.");
		//Don't change state as washer is now in use.
	}

	@Override
	public void ejectQuarter() {
		System.out.println("Cannot eject quarter when washer is in use. Please wait or cancel the load.");
		//Don't change state as washer is now in use.
	}

	@Override
	public void startLoad() {
		System.out.println("Load is already running!");
		//Don't change state as washer is now in use.
	}

	@Override
	public void cancelLoad() {
		washingMachine.timerCancel(); //Stop printing if it is happening. 
		System.out.println("Your load has been cancelled. You will NOT recieve a refund.");
		washingMachine.interuptAllTasks(0);
		
		
		washingMachine.setState(washingMachine.getNotFullyPaidState());
	}
	
	@Override
	public void doOperation() {
		washingMachine.addTaskToTimer(new WaitTask(), 6*1000);
		
	}

	
	private class WaitTask extends TimerTask{
		
		@Override
		public void run() {
						
			System.out.println("\nWashing!");
			washingMachine.addTaskToTimer(new PrintDotTask(), 0);
			washingMachine.addTaskToTimer(new PrintDotTask(), 1000);
			washingMachine.addTaskToTimer(new PrintDotTask(), 2000);
			washingMachine.addTaskToTimer(new PrintDotTask(), 3000);
			washingMachine.addTaskToTimer(new PrintDotTask(), 4000);
			washingMachine.addTaskToTimer(new PrintDotTask(), 5000);
			washingMachine.addTaskToTimer(new PrintDotTask(), 6000);
			
			washingMachine.setState(washingMachine.getRinseState());
			
		}
		
		private class PrintDotTask extends TimerTask{

			@Override
			public void run() {
				System.out.print(".");
				
			}
			
		}
		
	}
	
	@Override
	public String toString() {
		return "Washing";
	}


	

}
