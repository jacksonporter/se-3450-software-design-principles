public class HasPaidState extends State {

	public HasPaidState(WashingMachine washingMachine) {
		super(washingMachine);
	}

	@Override
	public void insertQuarter() {
		System.out.println("You can't insert a quarter. You have already inserted 4 quarters!");
		//Don't change state as we can't insert a quarter and are fully paid.
	}

	@Override
	public void ejectQuarter() {
		washingMachine.ejectQuarterFromState();
		System.out.println("Ejecting a quarter. There are now " + washingMachine.getNumQuarters() + " in the machine.");
		washingMachine.setState(washingMachine.getNotFullyPaidState());
	}

	@Override
	public void startLoad() {
		System.out.println("Staring load!");
		washingMachine.setState(washingMachine.getInUseState());
	}

	@Override
	public void cancelLoad() {
		System.out.println("Cannot cancel load if you haven't started one!");
		//Don't change state as we can't cancel a load because it hasn't started and are fully paid.
	}

	@Override
	public void doOperation() {
		//nothing to do!		
	}

	@Override
	public String toString() {
		return "HasPaid";
	}

}
