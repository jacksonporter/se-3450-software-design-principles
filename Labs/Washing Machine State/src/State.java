import java.io.*;

/**
 * This is the State Interface. It defines the basic operations that a washer can do and each subclass of this interface will implement the state change. 
 * @author jacksonporter
 *
 */
public abstract class State implements Serializable {
	transient WashingMachine washingMachine;
	
	public State(WashingMachine washingMachine) {
		this.washingMachine = washingMachine;
	}
	
	public abstract void insertQuarter();
	
	public abstract void ejectQuarter();
	
	public abstract void startLoad();
	
	public abstract void cancelLoad();

	public abstract void doOperation();
	
	//Eventually add an internal method to support the break down of a washer.
	
	@Override
	public abstract String toString();
}
