import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class WashingMachineRemoteDriver {

	public static void main(String[] args) 
	{
		try {
			Registry registry = LocateRegistry.getRegistry(null);
			
			WashingMachineRemoteInterface remote1 = (WashingMachineRemoteInterface) registry.lookup("remote1");
			
			System.out.println("Current State of Washer: " + remote1.getStateString());		
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
